package summer;
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

/**
 *
 * @author hgoody88
 */
public class Summer {
    
    private static final Scanner scanner = new Scanner(System.in);
    private static boolean flag;
    private static int B = scanner.nextInt();
    private static int H = scanner.nextInt();
    
    static {
        scanner.close();
        if((B <= 0) || (H <= 0)) {
            flag = false;
            System.out.println("Breadth and height must be positive");
        }        
    }    
    

    /**
     * @param args the command line arguments
     * scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
     */
    public static void main(String[] args) {
        if(flag){
            System.out.println("here");
	        int area=B*H;
		System.out.print(area);
	}
		
    }//end of main
}