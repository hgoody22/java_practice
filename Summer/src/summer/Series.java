package summer;

/**
 * Create math series: (a+2^0*b), (a+2^0*b + 2^1*b), ...        
 * @author hgoody88
 */
public class Series {
    private int a, b, n;
    private int[] sum;
    
    //Set up object
    Series(int a, int b, int n){
        this.a = a;
        this.b = b;
        this.n = n;
        this.sum = new int[n];
    }
    
    //get the resulting series in the array sum
    public int[] getSum(){
        createSeries();
        return sum;
    }
    
    //Sets the array of sum to each value in the series
    private void createSeries(){
        sum[0] = a + b;
        for(int i = 1; i < n; i++){            
            sum[i] = ((int) (Math.pow(2, i))*b) + sum[i-1];   
        }
    }
}
